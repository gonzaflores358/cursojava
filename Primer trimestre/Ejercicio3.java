public class Ejercicio3 {
	public static void main(String[] args) {
		
		System.out.println("Tecla de escape\t\tSignificado\n");
		System.out.println("\\n            \t\tSignifica una nueva linea");
		System.out.println("\\t            \t\tSignifica un tab de espacio");
		System.out.println("\\\"           \t\tEs para poner \" (comillas dobles) dentro del texto por ejemplo:");
		System.out.println("               \t\t\"Belencita\"");
		System.out.println("\\             \t\tSe utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\");
		System.out.println("\\\'           \t\tSe utiliza para las \'(comilla simple) para escribir por ejmeplo \'Princesita\'");
		
	}
}