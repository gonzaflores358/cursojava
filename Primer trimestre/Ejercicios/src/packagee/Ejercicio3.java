package packagee;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Ejercicio3 {

	private JFrame frame;
	private JTextField txtMes;
	private JLabel labelDias;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio3 window = new Ejercicio3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 198);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel labelMes = new JLabel("Inserte un mes");
		labelMes.setHorizontalAlignment(SwingConstants.CENTER);
		labelMes.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		labelMes.setBounds(10, 11, 414, 25);
		frame.getContentPane().add(labelMes);
		
		
		JLabel labelDias = new JLabel("");
		labelDias.setHorizontalAlignment(SwingConstants.CENTER);
		labelDias.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		labelDias.setBounds(10, 99, 414, 55);
		frame.getContentPane().add(labelDias);
		
		txtMes = new JTextField();
		txtMes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				
				String mes = txtMes.getText().toLowerCase();
				
				if (mes.equals(new String ("enero"))) {
					labelDias.setText("31 dias");
				} else if (mes.equals(new String ("febrero"))) {
					labelDias.setText("29 dias");
				} else if (mes.equals(new String ("marzo"))) {
					labelDias.setText("31 dias");
				} else if (mes.equals(new String ("mayo"))) {
					labelDias.setText("31 dias");
				}else if (mes.equals(new String ("junio"))) {
					labelDias.setText("30 dias");
				}else if (mes.equals(new String ("julio"))) {
					labelDias.setText("31 dias");
				}else if (mes.equals(new String ("agosto"))) {
					labelDias.setText("31 dias");
				}else if (mes.equals(new String ("septiembre"))) {
					labelDias.setText("30 dias");
				}else if (mes.equals(new String ("octubre"))) {
					labelDias.setText("31 dias");
				}else if (mes.equals(new String ("noviembre"))) {
					labelDias.setText("30 dias");
				}else if (mes.equals(new String ("diciembre"))) {
					labelDias.setText("31 dias");
				}else if (mes.equals(new String ("abril"))) {
					labelDias.setText("30 dias");
				} else {
					labelDias.setText("Porfavor inserte un mes");
				}
			}
		});
		txtMes.setBounds(161, 63, 100, 25);
		frame.getContentPane().add(txtMes);
		txtMes.setColumns(10);
		
		
	}
}
