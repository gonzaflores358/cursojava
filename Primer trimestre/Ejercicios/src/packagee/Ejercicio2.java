package packagee;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Ejercicio2 {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio2 window = new Ejercicio2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
	}

	/**
	 * Create the application.
	 */
	public Ejercicio2() {
		initialize();
		
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		frame = new JFrame();
		
		frame.setBounds(100, 100, 450, 157);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Inserte un numero");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel.setBounds(93, 11, 220, 20);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNumero = new JLabel("");
		lblNumero.setHorizontalAlignment(SwingConstants.CENTER);
		lblNumero.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		lblNumero.setBounds(10, 78, 414, 20);
		frame.getContentPane().add(lblNumero);
		
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
										
				try {
					int numero = Integer.parseInt(textField.getText());
					if(numero%2 == 0){
						lblNumero.setText("El numero es par");
					} else{
						lblNumero.setText("El numero no es par");
					}
					
				} catch(NumberFormatException nfe){
					lblNumero.setText("Inserte un numero");lblNumero.setText("Inserte ssun numero");
				}
			}
		});
		
		textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField.setBounds(160, 42, 100, 25);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
	}
}
