package packagee;

import java.awt.EventQueue;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio1 {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio1 window = new Ejercicio1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 183);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNota = new JLabel("Nota 1:");
		lblNota.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNota.setBounds(10, 11, 80, 20);
		frame.getContentPane().add(lblNota);
		
		JLabel lblNota_1 = new JLabel("Nota 2:");
		lblNota_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNota_1.setBounds(10, 42, 80, 20);
		frame.getContentPane().add(lblNota_1);
		
		JLabel lblNota_2 = new JLabel("Nota 3:");
		lblNota_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNota_2.setBounds(10, 73, 80, 20);
		frame.getContentPane().add(lblNota_2);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField.setBounds(84, 11, 90, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField_1.setColumns(10);
		textField_1.setBounds(84, 42, 90, 20);
		frame.getContentPane().add(textField_1);
							
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(208, 11, 216, 82);
		Border border = BorderFactory.createLineBorder(Color.BLACK, 2);
		lblNewLabel.setBorder(border);
		frame.getContentPane().add(lblNewLabel);
		
		textField_3 = new JTextField();
		textField_3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField_3.setColumns(10);
		textField_3.setBounds(84, 73, 90, 20);
		frame.getContentPane().add(textField_3);
		
		JButton btnNewButton = new JButton("Calcular nota");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String texto = textField.getText();
				int n1 = Integer.parseInt(textField.getText());
				int n2 = Integer.parseInt(textField_1.getText());
				int n3 = Integer.parseInt(textField_3.getText());

				if(((n1+n2+n3)/3) >= 7){
					lblNewLabel.setText("Aprobado");
					Border border = BorderFactory.createLineBorder(Color.GREEN, 2);
					lblNewLabel.setBorder(border);
				} else {
					lblNewLabel.setText("Desaprobado");
					Border border = BorderFactory.createLineBorder(Color.RED, 2);
					lblNewLabel.setBorder(border);
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton.setBounds(10, 104, 414, 30);
		frame.getContentPane().add(btnNewButton);
		
	}
}
