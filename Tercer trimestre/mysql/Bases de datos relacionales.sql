#2
create database java2_gonzalo_flores;

#3
use java2_gonzalo_flores;

#4
create table Provincias(PRO_ID int, PRO_DESCRIPCION varchar(50));
alter table Provincias add primary key (PRO_ID);

create table Partidos(PAR_ID int primary key, PRO_ID int, FOREIGN KEY (PRO_ID) REFERENCES Provincias(PRO_ID),PAR_DESCRIPCION varchar(50));

#5
insert into Provincias(PRO_ID, PRO_DESCRIPCION) values(1, 'Buenos Aires');
insert into Provincias(PRO_ID, PRO_DESCRIPCION) values(2, 'Cordoba');
insert into Provincias(PRO_ID, PRO_DESCRIPCION) values(3, 'Entre Rios');
insert into Provincias(PRO_ID, PRO_DESCRIPCION) values(4, 'Mendoza');

insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(1, 1, 'Moron');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(2, 1, 'La matanza');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(3, 1, 'Alberdi');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(4, 1, 'Avellaneda');

insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(5, 2, 'Capital');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(6, 2, 'Guaymallen');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(7, 2, 'Las heras');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(8, 2, 'Lavalle');

insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(16, 4, 'Calamuchita');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(15, 4, 'Capital');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(13, 4, 'Colon');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(14, 4, 'General Roca');

insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(9, 3, 'Colon');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(10, 3, 'Concordia');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(11, 3, 'Diamante');
insert into Partidos(PAR_ID, PRO_ID, PAR_DESCRIPCION) values(12, 3, 'Federacion');

#6
delete from Provincias where PRO_ID = 1; #No funciona debido a que no se puede elminiar una fila padre

delete from Partidos where PRO_ID = 4; 
delete from Provincias where PRO_ID = 4; # En este caso funciona debido a que primero se borraron las filas hijas

#7
update Provincias set PRO_DESCRIPCION = 'Bs. As' where PRO_ID = 1; 

update Partidos set PAR_DESCRIPCION = 'El mejor' where PAR_ID = 2;
update Provincias set PRO_DESCRIPCION = 'Buenos mates' where PRO_ID = 3;
update Provincias set PRO_DESCRIPCION = 'CBA' where PRO_ID = 2;

